#!/bin/bash

# Source setup.sh
setup_script=""

script_dir="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
search_dir=$script_dir
while [[ ${setup_script} == "" ]]
do
  setup_script=$( find $search_dir -name "setup.sh" )
  search_dir=$(dirname $search_dir)
done
if [[ ${setup_script} == "" ]]; then
  echo "ERROR: setup.sh not found"
  exit 1
fi
source $setup_script

"${COMP_PATH_Scripts:-../../../tools/bin}"/build_apk.py --apk-directory "${script_dir}"/ --ta "${script_dir}"/../taPinpad --ca "${script_dir}"/caPinpad

