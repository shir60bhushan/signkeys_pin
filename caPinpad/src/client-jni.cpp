/*
 * Copyright (c) 2013-2017 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

#include <string.h>
#include <stdint.h>
#include <jni.h>

#include "tee_client_api.h"
#include "caPinpad.h"

#define LOG_TAG "caPinpad:jni"
#include "log.h"

#define JAVA_CA_WRAPPER_CLASS "com/trustonic/sample/pinpad/ca/PinpadCAWrapper"

/* See for more help about JNI:
 * http://java.sun.com/docs/books/jni/html/jniTOC.html
 * http://java.sun.com/developer/onlineTraining/Programming/JDCBook/jni.html
 * http://developer.android.com/training/articles/perf-jni.html
 */

#ifdef __cplusplus
#define EXTERN_C extern "C"
#else
#define EXTERN_C
#endif

JNIEnv *gEnv = NULL;
JavaVM *gVm = NULL;
jclass gPinpadCAWrapper = NULL;

EXTERN_C JNIEXPORT bool JNICALL
Java_com_trustonic_sample_pinpad_ca_PinpadCAWrapper_caPinpadConnect(JNIEnv *env,
        jobject __attribute__((unused)) obj , jobject applicationContext) {

    LOG_D("pinpadConnect: calling securePinpadConnect()");
    /*
     * Register the Application Context so that we can bind to the Tui Service
     */
    JavaVM *jvm = NULL;
    env->GetJavaVM(&jvm);
    TEEC_TT_RegisterPlatformContext(jvm, applicationContext);
    return securePinpadConnect();
}

EXTERN_C JNIEXPORT void JNICALL
Java_com_trustonic_sample_pinpad_ca_PinpadCAWrapper_caPinpadDisconnect(JNIEnv __attribute__((unused)) *env,
        jobject __attribute__((unused)) obj) {

    LOG_D("pinpadDisconnect: calling securePinpadDisconnect()");
    CloseSession();
}

EXTERN_C JNIEXPORT bool JNICALL
Java_com_trustonic_sample_pinpad_ca_PinpadCAWrapper_caPinpadGetResolution(JNIEnv *env,
        jobject obj) {

    bool ret = false;
    TEEC_Result result;
    jfieldID fidScreenWidth = NULL;
    jfieldID fidScreenHeight = NULL;
    uint32_t width = 0;
    uint32_t height = 0;

    LOG_D("caPinpadGetResolution: calling securePinpadGetResolution()");
    result = securePinpadGetResolution(&width, &height);
    if (result != TEEC_SUCCESS) {
        return false;
    }

    if (gPinpadCAWrapper == NULL) {
        LOG_E("caPinpadGetResolution: missing parameter gPinpadCAWrapper");
        ret = false;
        goto exit;
    }

    gEnv = env;

    if (gEnv != NULL) {
        /* ------------------------------------------------------------- */
        /* Retrieve JNI information about the field width in the PinpadCAWrapper class */
        fidScreenWidth = gEnv->GetStaticFieldID(gPinpadCAWrapper, "screenWidth", "I");

        if (fidScreenWidth == NULL) {
            LOG_E("caPinpadGetResolution: cannot find field screenWidth");
            ret = false; /* field not found */
            goto exit;
        }

        gEnv->SetStaticIntField(gPinpadCAWrapper, fidScreenWidth, width);

        /* ------------------------------------------------------------- */
        /* Retrieve JNI information about the field height in the PinpadCAWrapper class */
        fidScreenHeight = gEnv->GetStaticFieldID(gPinpadCAWrapper, "screenHeight", "I");

        if (fidScreenHeight == NULL) {
            LOG_E("caPinpadGetResolution: cannot find field screenHeight");
            ret = false; /* field not found */
            goto exit;
        }

        gEnv->SetStaticIntField(gPinpadCAWrapper, fidScreenHeight, height);
        ret = true;
    }

exit:
    if (gEnv != NULL) {
        if (gEnv->ExceptionCheck()) {
            LOG_E("caPinpadGetResolution: java exception");
            gEnv->ExceptionClear();
        }
    } else {
        LOG_E("caPinpadGetResolution: exit gEnv is NULL");
    }

    return ret;
}

EXTERN_C JNIEXPORT bool JNICALL
Java_com_trustonic_sample_pinpad_ca_PinpadCAWrapper_caPinpadIsAlreadyProvisionned(JNIEnv __attribute__((unused)) *env,
        jobject __attribute__((unused)) obj,
        jint screenWidth) {

    bool isProvisioned = false;
    TEEC_Result result;
    result = securePinpadIsAlreadyProvisionned(screenWidth, &isProvisioned);
    if (result != TEEC_SUCCESS) {
        return false;
    }

    return isProvisioned;
}

EXTERN_C JNIEXPORT bool JNICALL
Java_com_trustonic_sample_pinpad_ca_PinpadCAWrapper_caPinpadProvisionItem(JNIEnv *env,
        jobject __attribute__((unused)) obj,
        jint screenWidth,
        jint itemID,
        jbyteArray item,
        jint itemLen,
        jint binType) {

    bool ret = false;
    jbyte *itemData = env->GetByteArrayElements(item, NULL);

    LOG_D("itemLen = %d", itemLen);
    ret = securePinpadProvisionItem(screenWidth, itemID, (char *)itemData, itemLen, binType);

    env->ReleaseByteArrayElements(item, itemData, 0);

    return ret;
}

EXTERN_C JNIEXPORT jstring JNICALL
Java_com_trustonic_sample_pinpad_ca_PinpadCAWrapper_caPinpadVerify(JNIEnv *env,
        jobject __attribute__((unused)) obj) {

    char *result;
    LOG_D("caPinpadVerify: calling securePinpadVerify()");
    result = (char *)securePinpadVerify(0);

    return env->NewStringUTF(result);
}

EXTERN_C JNIEXPORT jstring JNICALL
Java_com_trustonic_sample_pinpad_ca_PinpadCAWrapper_caPinpadDisplayMsg(JNIEnv *env,
        jobject obj,
        jstring path) {

    char* result;

    LOG_D("caPinpadDisplayMsg: calling securePinpadDisplayMsg()");
    result = (char*)securePinpadDisplayMsg();

    return gEnv->NewStringUTF(result);
}
jint JNI_OnLoad(JavaVM *vm, void __attribute__((unused)) *reserved) {
    JNIEnv *env;
    jclass pinpadCAWrapper;

    LOG_D("JNI_OnLoad");

    if (vm->GetEnv((void **) &env, JNI_VERSION_1_6) != JNI_OK) {
        LOG_E("JNI_OnLoad GetEnv failed");
        return -1;
    }

    pinpadCAWrapper = env->FindClass(JAVA_CA_WRAPPER_CLASS);
    if (pinpadCAWrapper == NULL) {
        LOG_E("FindClass on class PinpadCAWrapper failed!");
        return -1;
    }

    /* Cache the PinpadCAWrapper class in a global reference */
    /* Use the cached gPinpadCAWrapper.
     * As we called the AttachCurrentThread to get the java environnement from
     * a native thread, the FindClass will always fail. This is a ClassLoader issue.
     * This call (AttachCurrentThread) changes the call stack, so when the FindClass
     * try to start the class search in the class loader associated with this method,
     * FindClass find the ClassLoader associated with the a wrong class, so FindClass fails.*/
    gPinpadCAWrapper = (jclass) env->NewGlobalRef(pinpadCAWrapper);

    /* Cache the javaVM to get back a JNIEnv reference from native code*/
    gVm = vm;

    return JNI_VERSION_1_6;
}
