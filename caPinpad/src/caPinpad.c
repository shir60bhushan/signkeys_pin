/*
 * Copyright (c) 2016-2017 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

/**
 * Connector library for accessing TUI pinpad Trusted Application.
 */

#include <stdlib.h>
#include <errno.h>

#include "tee_client_api.h"
#include "caPinpad.h"

#include "taPinpadApi.h"
#include "taPinpad_uuid.h"

#define LOG_TAG "caPinpad:lib"
#include "log.h"

static const TEEC_UUID sUUID = taPinpad_UUID;

TEEC_Result nResult;
TEEC_Context sContext;
TEEC_Session sSession;
TEEC_Operation sOperation;
TEEC_SharedMemory sharedMem;

TEEC_Result OpenSession() {
    memset(&sContext, 0, sizeof(sContext));

    nResult = TEEC_InitializeContext(NULL, &sContext);
    if (nResult != TEEC_SUCCESS) {
        LOG_E("Could not initialize context with the TEE.");
        LOG_E("Failure");
        return (nResult);
    }

    memset(&sSession, 0, sizeof(sSession));
    memset(&sOperation, 0, sizeof(sOperation));

    nResult = TEEC_OpenSession(&sContext,
                               &sSession,
                               &sUUID,
                               TEEC_LOGIN_PUBLIC,
                               NULL, /* connectionData */
                               &sOperation, /* IN OUT operation */
                               NULL /* OUT returnOrigin, optional */);
    if (nResult != TEEC_SUCCESS) {
        LOG_E("Could not open session with Trusted Application.");
        LOG_E("Failure");
        return (nResult);
    }

    return (nResult);
}

bool securePinpadConnect() {
    TEEC_Result nResult;

    LOG_D("Opening the Trusted Application session");

    nResult = OpenSession();
    if (TEEC_SUCCESS != nResult) {
        LOG_E("Open session failed: %d", nResult);
        return false;
    }

    LOG_D("OpenSession() succeeded");
    return true;
}

TEEC_Result CloseSession() {
    TEEC_CloseSession(&sSession);  //the return should be verified ?
    memset(&sSession, 0, sizeof(sSession));
    return TEEC_SUCCESS;
}

TEEC_Result securePinpadIsAlreadyProvisionned(uint32_t screenWidth,
                                              bool *isProvisionned) {
    TEEC_Result nResult;

    memset(&sOperation, 0, sizeof(sOperation));

    sOperation.paramTypes = TEEC_PARAM_TYPES(TEEC_VALUE_INOUT,
                                             TEEC_NONE,
                                             TEEC_NONE,
                                             TEEC_NONE);
    sOperation.params[0].value.a = screenWidth;

    *isProvisionned = 0;
    nResult = TEEC_InvokeCommand(&sSession,
                                 CMD_ID_IS_ALREADY_PROVISIONNED,
                                 &sOperation,
                                 NULL);

    if (nResult != TEEC_SUCCESS) {
        LOG_E("securePinpadIsAlreadyProvisionned failed with 0x%x", nResult);
        return nResult;
    }

    *isProvisionned = !!sOperation.params[0].value.a; // convert to bool
    LOG_D("isProvisionned=%d", *isProvisionned);

    return nResult;
}

TEEC_Result securePinpadGetResolution(uint32_t *width, uint32_t *height) {
    LOG_D("Get Resolution");
    TEEC_Result nResult;

    memset(&sOperation, 0, sizeof(sOperation));

    sOperation.paramTypes = TEEC_PARAM_TYPES(TEEC_VALUE_OUTPUT,
                                             TEEC_NONE,
                                             TEEC_NONE,
                                             TEEC_NONE);

    nResult = TEEC_InvokeCommand(&sSession,
                                 CMD_ID_PINPAD_GET_RES,
                                 &sOperation,
                                 NULL);
    if (nResult != TEEC_SUCCESS) {
        LOG_E("Get Resolution failed with 0x%x", nResult);
        return nResult;
    }

    *width = sOperation.params[0].value.a;
    *height = sOperation.params[0].value.b;

    return nResult;
}

/** securePinpadProvisionItem() - securely provision data to TA
 *
 * @param data      Buffer containing the data
 * @param dataLen   Length of buffer
 *
 * @return true, if operation was successful, false otherwise
 */
bool securePinpadProvisionItem(int screenWidth, int itemID, char *data,
                               uint32_t dataLen, int binType) {
    TEEC_Result nResult;

    LOG_D("data = %p dataLen = %d MAX_FILE_SIZE = %d", data, dataLen, MAX_FILE_SIZE);
    LOG_D("Provisioning: for screenWidth %d, item %d, binType 0x%08x",
          screenWidth,
          itemID,
          binType);

    // -------------------------------------------------------------
    // Initialize the provisioning
    memset(&sOperation, 0, sizeof(sOperation));
    sOperation.paramTypes = TEEC_PARAM_TYPES(TEEC_VALUE_INPUT,
                                             TEEC_VALUE_INPUT,
                                             TEEC_NONE,
                                             TEEC_NONE);
    sOperation.params[0].value.a = screenWidth;
    sOperation.params[0].value.b = itemID;
    sOperation.params[1].value.a = binType;

    nResult = TEEC_InvokeCommand(&sSession,
                                 CMD_ID_START_ASSET_TRANSFER,
                                 &sOperation,
                                 NULL);
    if (nResult != TEEC_SUCCESS) {
        LOG_E("Provisioning: for screenWidth %d, item %d, binType 0x%08x",
              screenWidth,
              itemID,
              binType);
        LOG_E("Provision CMD_ID_START_ASSET_TRANSFER failed with 0x%x",
              nResult);
        return false;
    }

    // -------------------------------------------------------------
    // Provision the data, in chunks as needed
    char *chunkBuf = data;
    uint32_t chunkLen = 0;
    uint32_t chunksCnt = dataLen / MAX_FILE_SIZE;

    if (dataLen % MAX_FILE_SIZE) {
        chunksCnt++;
    }

    for (; chunksCnt > 0; chunksCnt--) {
        if (1 == chunksCnt) {
            chunkLen = dataLen % MAX_FILE_SIZE;
        } else {
            chunkLen = MAX_FILE_SIZE;
        }

        LOG_D("chunksCnt: %d", chunksCnt);
        LOG_D("chunkBuf: %p", chunkBuf);
        LOG_D("chunkLen: %d", chunkLen);

        memset(&sOperation, 0, sizeof(sOperation));
        sOperation.paramTypes = TEEC_PARAM_TYPES(TEEC_MEMREF_TEMP_INPUT,
                                                 TEEC_NONE,
                                                 TEEC_NONE,
                                                 TEEC_NONE);
        sOperation.params[0].tmpref.buffer = chunkBuf;
        sOperation.params[0].tmpref.size = chunkLen;

        nResult = TEEC_InvokeCommand(&sSession,
                                     CMD_ID_CONTINUE_ASSET_TRANSFER,
                                     &sOperation,
                                     NULL);
        if (nResult != TEEC_SUCCESS) {
            LOG_E("Provisioning: for screenWidth %d, item %d, binType 0x%08x",
                  screenWidth,
                  itemID,
                  binType);
            LOG_E("chunksCnt: %d", chunksCnt);
            LOG_E("chunkBuf: %p", chunkBuf);
            LOG_E("chunkLen: %d", chunkLen);
            LOG_E("Provision CMD_ID_CONTINUE_ASSET_TRANSFER failed with 0x%x", nResult);
            return false;
        }

        chunkBuf = chunkBuf + chunkLen;
    }

    // -------------------------------------------------------------
    // Finalize the provisioning
    memset(&sOperation, 0, sizeof(sOperation));
    sOperation.paramTypes = TEEC_PARAM_TYPES(TEEC_NONE,
                                             TEEC_NONE,
                                             TEEC_NONE,
                                             TEEC_NONE);
    nResult = TEEC_InvokeCommand(&sSession,
                                 CMD_ID_STOP_ASSET_TRANSFER,
                                 &sOperation,
                                 NULL);
    if (nResult != TEEC_SUCCESS) {
        LOG_E("Provisioning: for screenWidth %d, item %d, binType 0x%08x",
              screenWidth,
              itemID,
              binType);
        LOG_E("Provision CMD_ID_STOP_ASSET_TRANSFER failed with 0x%x", nResult);
        return false;
    }

    return true;
}

const char *securePinpadDisplayMsg() {
    TEEC_Result nResult;

    // -------------------------------------------------------------
    // Call Trusted Application
    memset(&sOperation, 0, sizeof(sOperation));

    sOperation.paramTypes = TEEC_PARAM_TYPES(TEEC_VALUE_OUTPUT,
                                             TEEC_NONE,
                                             TEEC_NONE,
                                             TEEC_NONE);

    nResult = TEEC_InvokeCommand(&sSession,
                                 CMD_ID_DISPLAY_MSG,
                                 &sOperation,
                                 NULL);

    uint32_t lauchResult = sOperation.params[0].value.a;

    // -------------------------------------------------------------
    // Handle Trusted Application response
    if (nResult != TEEC_SUCCESS) {
        LOG_E("Launch DisplayMsg failed with 0x%x", nResult);
        return "Launching DisplayMsg failed";
    }
    switch (lauchResult) {
    case RES_ID_DISPLAY_MSG_OK:
        LOG_D("Trusted Application Response: selected OK");
        return "selected OK";
    case RES_ID_DISPLAY_MSG_CANCELLED:
        LOG_D("Trusted Application Response: TUI session was cancelled");
        return "was cancelled";
    default:
        LOG_D("Trusted Application Response: Unknown response data %d",
              lauchResult);
        return "Unknown response";
    }
}

const char *securePinpadVerify(uint32_t resolutionID) {
    TEEC_Result nResult;

    // -------------------------------------------------------------
    // Call Trusted Application
    memset(&sOperation, 0, sizeof(sOperation));

    sOperation.paramTypes = TEEC_PARAM_TYPES(TEEC_VALUE_OUTPUT,
                                             TEEC_NONE,
                                             TEEC_NONE,
                                             TEEC_NONE);
    sOperation.params[0].value.a = resolutionID;

    nResult = TEEC_InvokeCommand(&sSession,
                                 CMD_ID_PINPAD_LAUNCH,
                                 &sOperation,
                                 NULL);

    uint32_t lauchResult = sOperation.params[0].value.a;

    // -------------------------------------------------------------
    // Handle Trusted Application response
    if (nResult != TEEC_SUCCESS) {
        LOG_E("Launch pinpad failed with 0x%x", nResult);
        return "Launching pinpad failed";
    }
    switch (lauchResult) {
    case RES_ID_PINPAD_PIN_OK:
        LOG_D("Trusted Application Response: PIN verification: correct");
        return "PIN verification. correct";
    case RES_ID_PINPAD_PIN_NOK:
        LOG_D("Trusted Application Response: PIN is not correct");
        return "PIN verification. incorrect PIN";
    case RES_ID_PINPAD_CANCELLED:
        LOG_D("Trusted Application Response: TUI session was cancelled");
        return "PIN verification. PIN entry was cancelled";
    default:
        LOG_D("Trusted Application Response: Unknown response data %d",
              lauchResult);
        return "PIN verification. Invalid response from Trusted Application";
    }
}
