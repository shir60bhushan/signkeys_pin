/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.trustonic.components.thpagent;

public final class R {
    public static final class integer {
        public static final int revision = 0x7f050000;
    }
    public static final class raw {
        public static final int lta = 0x7f08003a;
        public static final int mwgetat = 0x7f08003b;
        public static final int sdta = 0x7f080076;
    }
    public static final class string {
        public static final int branch = 0x7f090003;
    }
}
