/* AUTO-GENERATED FILE. DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found. It
 * should not be modified by hand.
 */

package com.trustonic.sample.pinpad;

public final class R {
  public static final class color {
    public static final int trustonic_blue=0x7f010000;
    public static final int trustonic_dark_blue=0x7f010001;
    public static final int trustonic_dark_grey=0x7f010002;
    public static final int trustonic_grey=0x7f010003;
    public static final int trustonic_light_blue=0x7f010004;
    public static final int trustonic_light_grey=0x7f010005;
    public static final int trustonic_very_dark_blue=0x7f010006;
    public static final int trustonic_very_light_grey=0x7f010007;
    public static final int white=0x7f010008;
  }
  public static final class dimen {
    public static final int activity_horizontal_margin=0x7f020000;
    public static final int activity_vertical_margin=0x7f020001;
  }
  public static final class drawable {
    public static final int button=0x7f030000;
    public static final int button_enabled=0x7f030001;
    public static final int button_pressed=0x7f030002;
    public static final int home_header_picture=0x7f030003;
    public static final int ic_launcher=0x7f030004;
    public static final int layout_header=0x7f030005;
    public static final int t_logo=0x7f030006;
  }
  public static final class id {
    public static final int action_settings=0x7f040000;
    public static final int display_msg_btn=0x7f040001;
    public static final int home_body_text=0x7f040002;
    public static final int home_header_layout=0x7f040003;
    public static final int home_header_picture=0x7f040004;
    public static final int home_pinpad_btn=0x7f040005;
    public static final int pinpad_ta_installation=0x7f040006;
    public static final int welcome_text=0x7f040007;
  }
  public static final class integer {
    public static final int revision=0x7f050000;
  }
  public static final class layout {
    public static final int activity_start_tui_pinpad=0x7f060000;
  }
  public static final class menu {
    public static final int start_tui_pinpad=0x7f070000;
  }
  public static final class raw {
    public static final int hd_blank=0x7f080000;
    public static final int hd_button_0=0x7f080001;
    public static final int hd_button_1=0x7f080002;
    public static final int hd_button_2=0x7f080003;
    public static final int hd_button_3=0x7f080004;
    public static final int hd_button_4=0x7f080005;
    public static final int hd_button_5=0x7f080006;
    public static final int hd_button_6=0x7f080007;
    public static final int hd_button_7=0x7f080008;
    public static final int hd_button_8=0x7f080009;
    public static final int hd_button_9=0x7f08000a;
    public static final int hd_button_cancel=0x7f08000b;
    public static final int hd_button_correction=0x7f08000c;
    public static final int hd_button_validate=0x7f08000d;
    public static final int hd_pinpad=0x7f08000e;
    public static final int hd_pressed_button_0=0x7f08000f;
    public static final int hd_pressed_button_1=0x7f080010;
    public static final int hd_pressed_button_2=0x7f080011;
    public static final int hd_pressed_button_3=0x7f080012;
    public static final int hd_pressed_button_4=0x7f080013;
    public static final int hd_pressed_button_5=0x7f080014;
    public static final int hd_pressed_button_6=0x7f080015;
    public static final int hd_pressed_button_7=0x7f080016;
    public static final int hd_pressed_button_8=0x7f080017;
    public static final int hd_pressed_button_9=0x7f080018;
    public static final int hd_pressed_button_cancel=0x7f080019;
    public static final int hd_pressed_button_correction=0x7f08001a;
    public static final int hd_pressed_button_validate=0x7f08001b;
    public static final int hd_star=0x7f08001c;
    public static final int hdready_blank=0x7f08001d;
    public static final int hdready_button_0=0x7f08001e;
    public static final int hdready_button_1=0x7f08001f;
    public static final int hdready_button_2=0x7f080020;
    public static final int hdready_button_3=0x7f080021;
    public static final int hdready_button_4=0x7f080022;
    public static final int hdready_button_5=0x7f080023;
    public static final int hdready_button_6=0x7f080024;
    public static final int hdready_button_7=0x7f080025;
    public static final int hdready_button_8=0x7f080026;
    public static final int hdready_button_9=0x7f080027;
    public static final int hdready_button_cancel=0x7f080028;
    public static final int hdready_button_correction=0x7f080029;
    public static final int hdready_button_validate=0x7f08002a;
    public static final int hdready_pinpad=0x7f08002b;
    public static final int hdready_pressed_button_0=0x7f08002c;
    public static final int hdready_pressed_button_1=0x7f08002d;
    public static final int hdready_pressed_button_2=0x7f08002e;
    public static final int hdready_pressed_button_3=0x7f08002f;
    public static final int hdready_pressed_button_4=0x7f080030;
    public static final int hdready_pressed_button_5=0x7f080031;
    public static final int hdready_pressed_button_6=0x7f080032;
    public static final int hdready_pressed_button_7=0x7f080033;
    public static final int hdready_pressed_button_8=0x7f080034;
    public static final int hdready_pressed_button_9=0x7f080035;
    public static final int hdready_pressed_button_cancel=0x7f080036;
    public static final int hdready_pressed_button_correction=0x7f080037;
    public static final int hdready_pressed_button_validate=0x7f080038;
    public static final int hdready_star=0x7f080039;
    public static final int lta=0x7f08003a;
    public static final int mwgetat=0x7f08003b;
    public static final int qhd_blank=0x7f08003c;
    public static final int qhd_button_0=0x7f08003d;
    public static final int qhd_button_1=0x7f08003e;
    public static final int qhd_button_2=0x7f08003f;
    public static final int qhd_button_3=0x7f080040;
    public static final int qhd_button_4=0x7f080041;
    public static final int qhd_button_5=0x7f080042;
    public static final int qhd_button_6=0x7f080043;
    public static final int qhd_button_7=0x7f080044;
    public static final int qhd_button_8=0x7f080045;
    public static final int qhd_button_9=0x7f080046;
    public static final int qhd_button_cancel=0x7f080047;
    public static final int qhd_button_correction=0x7f080048;
    public static final int qhd_button_validate=0x7f080049;
    public static final int qhd_pinpad=0x7f08004a;
    public static final int qhd_pressed_button_0=0x7f08004b;
    public static final int qhd_pressed_button_1=0x7f08004c;
    public static final int qhd_pressed_button_2=0x7f08004d;
    public static final int qhd_pressed_button_3=0x7f08004e;
    public static final int qhd_pressed_button_4=0x7f08004f;
    public static final int qhd_pressed_button_5=0x7f080050;
    public static final int qhd_pressed_button_6=0x7f080051;
    public static final int qhd_pressed_button_7=0x7f080052;
    public static final int qhd_pressed_button_8=0x7f080053;
    public static final int qhd_pressed_button_9=0x7f080054;
    public static final int qhd_pressed_button_cancel=0x7f080055;
    public static final int qhd_pressed_button_correction=0x7f080056;
    public static final int qhd_pressed_button_validate=0x7f080057;
    public static final int qhd_star=0x7f080058;
    public static final int quarthd_blank=0x7f080059;
    public static final int quarthd_button_0=0x7f08005a;
    public static final int quarthd_button_1=0x7f08005b;
    public static final int quarthd_button_2=0x7f08005c;
    public static final int quarthd_button_3=0x7f08005d;
    public static final int quarthd_button_4=0x7f08005e;
    public static final int quarthd_button_5=0x7f08005f;
    public static final int quarthd_button_6=0x7f080060;
    public static final int quarthd_button_7=0x7f080061;
    public static final int quarthd_button_8=0x7f080062;
    public static final int quarthd_button_9=0x7f080063;
    public static final int quarthd_button_cancel=0x7f080064;
    public static final int quarthd_button_correction=0x7f080065;
    public static final int quarthd_button_validate=0x7f080066;
    public static final int quarthd_pinpad=0x7f080067;
    public static final int quarthd_pressed_button_0=0x7f080068;
    public static final int quarthd_pressed_button_1=0x7f080069;
    public static final int quarthd_pressed_button_2=0x7f08006a;
    public static final int quarthd_pressed_button_3=0x7f08006b;
    public static final int quarthd_pressed_button_4=0x7f08006c;
    public static final int quarthd_pressed_button_5=0x7f08006d;
    public static final int quarthd_pressed_button_6=0x7f08006e;
    public static final int quarthd_pressed_button_7=0x7f08006f;
    public static final int quarthd_pressed_button_8=0x7f080070;
    public static final int quarthd_pressed_button_9=0x7f080071;
    public static final int quarthd_pressed_button_cancel=0x7f080072;
    public static final int quarthd_pressed_button_correction=0x7f080073;
    public static final int quarthd_pressed_button_validate=0x7f080074;
    public static final int quarthd_star=0x7f080075;
    public static final int sdta=0x7f080076;
    public static final int wsvga_blank=0x7f080077;
    public static final int wsvga_button_0=0x7f080078;
    public static final int wsvga_button_1=0x7f080079;
    public static final int wsvga_button_2=0x7f08007a;
    public static final int wsvga_button_3=0x7f08007b;
    public static final int wsvga_button_4=0x7f08007c;
    public static final int wsvga_button_5=0x7f08007d;
    public static final int wsvga_button_6=0x7f08007e;
    public static final int wsvga_button_7=0x7f08007f;
    public static final int wsvga_button_8=0x7f080080;
    public static final int wsvga_button_9=0x7f080081;
    public static final int wsvga_button_cancel=0x7f080082;
    public static final int wsvga_button_correction=0x7f080083;
    public static final int wsvga_button_validate=0x7f080084;
    public static final int wsvga_pinpad=0x7f080085;
    public static final int wsvga_pressed_button_0=0x7f080086;
    public static final int wsvga_pressed_button_1=0x7f080087;
    public static final int wsvga_pressed_button_2=0x7f080088;
    public static final int wsvga_pressed_button_3=0x7f080089;
    public static final int wsvga_pressed_button_4=0x7f08008a;
    public static final int wsvga_pressed_button_5=0x7f08008b;
    public static final int wsvga_pressed_button_6=0x7f08008c;
    public static final int wsvga_pressed_button_7=0x7f08008d;
    public static final int wsvga_pressed_button_8=0x7f08008e;
    public static final int wsvga_pressed_button_9=0x7f08008f;
    public static final int wsvga_pressed_button_cancel=0x7f080090;
    public static final int wsvga_pressed_button_correction=0x7f080091;
    public static final int wsvga_pressed_button_validate=0x7f080092;
    public static final int wsvga_star=0x7f080093;
  }
  public static final class string {
    public static final int action_settings=0x7f090000;
    public static final int alert_title=0x7f090001;
    public static final int app_name=0x7f090002;
    public static final int branch=0x7f090003;
    public static final int ca_cert_raw=0x7f090004;
    public static final int display_msg_btn=0x7f090005;
    public static final int home_actionbar_subtitle=0x7f090006;
    public static final int home_actionbar_title=0x7f090007;
    public static final int home_body_text=0x7f090008;
    public static final int home_header_title=0x7f090009;
    public static final int home_pinpad_btn=0x7f09000a;
    public static final int pinpad_mode_hint=0x7f09000b;
    public static final int tam_server_url=0x7f09000c;
    public static final int welcome_text=0x7f09000d;
  }
  public static final class style {
    public static final int AppBaseTheme=0x7f0a0000;
    public static final int AppTheme=0x7f0a0001;
    public static final int MyActionBar=0x7f0a0002;
    public static final int trustonicButton=0x7f0a0003;
  }
  public static final class xml {
    public static final int knoxenabled_info=0x7f0b0000;
  }
}