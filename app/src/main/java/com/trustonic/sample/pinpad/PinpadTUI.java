/*
 * Copyright (c) 2013-2017 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

package com.trustonic.sample.pinpad;

import android.content.Context;

import com.trustonic.sample.pinpad.BuildConfig;
import com.trustonic.sample.pinpad.Screen.ResolutionType;
import com.trustonic.util.tLog;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.EnumMap;

public class PinpadTUI {
    private static final String TAG = PinpadTUI.class.getSimpleName();

    /*
     * This is a singleton
     */
    private static final PinpadTUI instance = new PinpadTUI();
    private boolean startRequestPending = false;

    private static EnumMap<ResolutionType, ResolutionImage> resTable;

    private static class ResolutionImage {
        private ArrayList<Integer> listOfImage;
        private ArrayList<String> listOfFont;

        public ResolutionImage(ResolutionType resType) {

            listOfImage = new ArrayList<Integer>();
            listOfFont = new ArrayList<String>();

            if (BuildConfig.featureDataFontEnabled.equals("Yes")) {
                listOfFont.add("Roboto-Light.ttf");
                listOfFont.add("Roboto-Medium.ttf");
            }

            switch (resType) {
                case LOW:
                    tLog.d(TAG, "Low resolution screen.");
                    listOfImage.add(R.raw.quarthd_blank);
                    listOfImage.add(R.raw.quarthd_button_0);
                    listOfImage.add(R.raw.quarthd_button_1);
                    listOfImage.add(R.raw.quarthd_button_2);
                    listOfImage.add(R.raw.quarthd_button_3);
                    listOfImage.add(R.raw.quarthd_button_4);
                    listOfImage.add(R.raw.quarthd_button_5);
                    listOfImage.add(R.raw.quarthd_button_6);
                    listOfImage.add(R.raw.quarthd_button_7);
                    listOfImage.add(R.raw.quarthd_button_8);
                    listOfImage.add(R.raw.quarthd_button_9);
                    listOfImage.add(R.raw.quarthd_button_cancel);
                    listOfImage.add(R.raw.quarthd_button_correction);
                    listOfImage.add(R.raw.quarthd_button_validate);
                    listOfImage.add(R.raw.quarthd_pinpad);
                    listOfImage.add(R.raw.quarthd_pressed_button_0);
                    listOfImage.add(R.raw.quarthd_pressed_button_1);
                    listOfImage.add(R.raw.quarthd_pressed_button_2);
                    listOfImage.add(R.raw.quarthd_pressed_button_3);
                    listOfImage.add(R.raw.quarthd_pressed_button_4);
                    listOfImage.add(R.raw.quarthd_pressed_button_5);
                    listOfImage.add(R.raw.quarthd_pressed_button_6);
                    listOfImage.add(R.raw.quarthd_pressed_button_7);
                    listOfImage.add(R.raw.quarthd_pressed_button_8);
                    listOfImage.add(R.raw.quarthd_pressed_button_9);
                    listOfImage.add(R.raw.quarthd_pressed_button_cancel);
                    listOfImage.add(R.raw.quarthd_pressed_button_correction);
                    listOfImage.add(R.raw.quarthd_pressed_button_validate);
                    listOfImage.add(R.raw.quarthd_star);
                    break;

                case MEDIUM:
                    tLog.d(TAG, "Medium resolution screen.");
                    listOfImage.add(R.raw.wsvga_blank);
                    listOfImage.add(R.raw.wsvga_button_0);
                    listOfImage.add(R.raw.wsvga_button_1);
                    listOfImage.add(R.raw.wsvga_button_2);
                    listOfImage.add(R.raw.wsvga_button_3);
                    listOfImage.add(R.raw.wsvga_button_4);
                    listOfImage.add(R.raw.wsvga_button_5);
                    listOfImage.add(R.raw.wsvga_button_6);
                    listOfImage.add(R.raw.wsvga_button_7);
                    listOfImage.add(R.raw.wsvga_button_8);
                    listOfImage.add(R.raw.wsvga_button_9);
                    listOfImage.add(R.raw.wsvga_button_cancel);
                    listOfImage.add(R.raw.wsvga_button_correction);
                    listOfImage.add(R.raw.wsvga_button_validate);
                    listOfImage.add(R.raw.wsvga_pinpad);
                    listOfImage.add(R.raw.wsvga_pressed_button_0);
                    listOfImage.add(R.raw.wsvga_pressed_button_1);
                    listOfImage.add(R.raw.wsvga_pressed_button_2);
                    listOfImage.add(R.raw.wsvga_pressed_button_3);
                    listOfImage.add(R.raw.wsvga_pressed_button_4);
                    listOfImage.add(R.raw.wsvga_pressed_button_5);
                    listOfImage.add(R.raw.wsvga_pressed_button_6);
                    listOfImage.add(R.raw.wsvga_pressed_button_7);
                    listOfImage.add(R.raw.wsvga_pressed_button_8);
                    listOfImage.add(R.raw.wsvga_pressed_button_9);
                    listOfImage.add(R.raw.wsvga_pressed_button_cancel);
                    listOfImage.add(R.raw.wsvga_pressed_button_correction);
                    listOfImage.add(R.raw.wsvga_pressed_button_validate);
                    listOfImage.add(R.raw.wsvga_star);
                    break;


                case MIDHIGH:
                    tLog.d(TAG, "Mid High resolution screen.");
                    listOfImage.add(R.raw.hdready_blank);
                    listOfImage.add(R.raw.hdready_button_0);
                    listOfImage.add(R.raw.hdready_button_1);
                    listOfImage.add(R.raw.hdready_button_2);
                    listOfImage.add(R.raw.hdready_button_3);
                    listOfImage.add(R.raw.hdready_button_4);
                    listOfImage.add(R.raw.hdready_button_5);
                    listOfImage.add(R.raw.hdready_button_6);
                    listOfImage.add(R.raw.hdready_button_7);
                    listOfImage.add(R.raw.hdready_button_8);
                    listOfImage.add(R.raw.hdready_button_9);
                    listOfImage.add(R.raw.hdready_button_cancel);
                    listOfImage.add(R.raw.hdready_button_correction);
                    listOfImage.add(R.raw.hdready_button_validate);
                    listOfImage.add(R.raw.hdready_pinpad);
                    listOfImage.add(R.raw.hdready_pressed_button_0);
                    listOfImage.add(R.raw.hdready_pressed_button_1);
                    listOfImage.add(R.raw.hdready_pressed_button_2);
                    listOfImage.add(R.raw.hdready_pressed_button_3);
                    listOfImage.add(R.raw.hdready_pressed_button_4);
                    listOfImage.add(R.raw.hdready_pressed_button_5);
                    listOfImage.add(R.raw.hdready_pressed_button_6);
                    listOfImage.add(R.raw.hdready_pressed_button_7);
                    listOfImage.add(R.raw.hdready_pressed_button_8);
                    listOfImage.add(R.raw.hdready_pressed_button_9);
                    listOfImage.add(R.raw.hdready_pressed_button_cancel);
                    listOfImage.add(R.raw.hdready_pressed_button_correction);
                    listOfImage.add(R.raw.hdready_pressed_button_validate);
                    listOfImage.add(R.raw.hdready_star);
                    break;

                case HIGH:
                    tLog.d(TAG, "High resolution screen.");
                    listOfImage.add(R.raw.hd_blank);
                    listOfImage.add(R.raw.hd_button_0);
                    listOfImage.add(R.raw.hd_button_1);
                    listOfImage.add(R.raw.hd_button_2);
                    listOfImage.add(R.raw.hd_button_3);
                    listOfImage.add(R.raw.hd_button_4);
                    listOfImage.add(R.raw.hd_button_5);
                    listOfImage.add(R.raw.hd_button_6);
                    listOfImage.add(R.raw.hd_button_7);
                    listOfImage.add(R.raw.hd_button_8);
                    listOfImage.add(R.raw.hd_button_9);
                    listOfImage.add(R.raw.hd_button_cancel);
                    listOfImage.add(R.raw.hd_button_correction);
                    listOfImage.add(R.raw.hd_button_validate);
                    listOfImage.add(R.raw.hd_pinpad);
                    listOfImage.add(R.raw.hd_pressed_button_0);
                    listOfImage.add(R.raw.hd_pressed_button_1);
                    listOfImage.add(R.raw.hd_pressed_button_2);
                    listOfImage.add(R.raw.hd_pressed_button_3);
                    listOfImage.add(R.raw.hd_pressed_button_4);
                    listOfImage.add(R.raw.hd_pressed_button_5);
                    listOfImage.add(R.raw.hd_pressed_button_6);
                    listOfImage.add(R.raw.hd_pressed_button_7);
                    listOfImage.add(R.raw.hd_pressed_button_8);
                    listOfImage.add(R.raw.hd_pressed_button_9);
                    listOfImage.add(R.raw.hd_pressed_button_cancel);
                    listOfImage.add(R.raw.hd_pressed_button_correction);
                    listOfImage.add(R.raw.hd_pressed_button_validate);
                    listOfImage.add(R.raw.hd_star);
                    break;

                case EXTRA_HIGH:
                    tLog.d(TAG, "Extra High resolution screen.");
                    listOfImage.add(R.raw.qhd_blank);
                    listOfImage.add(R.raw.qhd_button_0);
                    listOfImage.add(R.raw.qhd_button_1);
                    listOfImage.add(R.raw.qhd_button_2);
                    listOfImage.add(R.raw.qhd_button_3);
                    listOfImage.add(R.raw.qhd_button_4);
                    listOfImage.add(R.raw.qhd_button_5);
                    listOfImage.add(R.raw.qhd_button_6);
                    listOfImage.add(R.raw.qhd_button_7);
                    listOfImage.add(R.raw.qhd_button_8);
                    listOfImage.add(R.raw.qhd_button_9);
                    listOfImage.add(R.raw.qhd_button_cancel);
                    listOfImage.add(R.raw.qhd_button_correction);
                    listOfImage.add(R.raw.qhd_button_validate);
                    listOfImage.add(R.raw.qhd_pinpad);
                    listOfImage.add(R.raw.qhd_pressed_button_0);
                    listOfImage.add(R.raw.qhd_pressed_button_1);
                    listOfImage.add(R.raw.qhd_pressed_button_2);
                    listOfImage.add(R.raw.qhd_pressed_button_3);
                    listOfImage.add(R.raw.qhd_pressed_button_4);
                    listOfImage.add(R.raw.qhd_pressed_button_5);
                    listOfImage.add(R.raw.qhd_pressed_button_6);
                    listOfImage.add(R.raw.qhd_pressed_button_7);
                    listOfImage.add(R.raw.qhd_pressed_button_8);
                    listOfImage.add(R.raw.qhd_pressed_button_9);
                    listOfImage.add(R.raw.qhd_pressed_button_cancel);
                    listOfImage.add(R.raw.qhd_pressed_button_correction);
                    listOfImage.add(R.raw.qhd_pressed_button_validate);
                    listOfImage.add(R.raw.qhd_star);
                    break;

                default:
                    tLog.e(TAG, "ERROR: no resources available for this screen resolution = " + resType);
                    break;
            }
        }

        ArrayList<Integer> getImageList() {
            return listOfImage;
        }

        ArrayList<String> getFontList() {
            return listOfFont;
        }
    }

    ;

    public static PinpadTUI getInstance() {
        return instance;
    }

    private PinpadTUI() {
        startRequestPending = false;
        resTable = new EnumMap<ResolutionType, ResolutionImage>(ResolutionType.class);

        for (ResolutionType resType : ResolutionType.values()) {
            resTable.put(resType, new ResolutionImage(resType));
        }
    }

    private byte[] getRawRessource(Context appContext, int id) {
        InputStream inStream = appContext.getResources().openRawResource(id);
        int imageLen;
        byte[] buffer = null;

        try {
            imageLen = inStream.available();
            buffer = new byte[imageLen];
            inStream.read(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            inStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return buffer;
    }

    public byte[] getRawImage(Context appContext, ResolutionType resType, int id) {
        return getRawRessource(appContext, resTable.get(resType).getImageList().get(id));
    }

    public ArrayList<Integer> getImageForResolution(ResolutionType resType) {
        return resTable.get(resType).getImageList();
    }

    public byte[] getRawFont(Context appContext, ResolutionType resType, int id) {
        byte buffer[];

        try {
            tLog.d(TAG, "Fonts name is " + resTable.get(resType).getFontList().get(id)
                   + " id = " + id);
            InputStream is = appContext.getAssets().open(resTable.get(resType).getFontList().get(id));
            buffer = new byte[is.available()];
            is.read(buffer);
            is.close();
            tLog.i(TAG, "Fonts are available");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            tLog.e(TAG, "Fonts are not available");
            return null;
        }

        return buffer;
    }

    public ArrayList<String> getFontForResolution(ResolutionType resType) {
        return resTable.get(resType).getFontList();
    }
}
