/*
 * Copyright (c) 2014-2017 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */
package com.trustonic.sample.pinpad;

import android.app.Activity;
import android.util.Log;
import android.widget.TextView;

import com.trustonic.components.thpagent.agent.TEEMode;
import com.trustonic.components.thpagent.agent.THPAgent;
import com.trustonic.components.thpagent.event.Outcome;
import com.trustonic.components.thpagent.listener.InstallTAListener;

import java.security.cert.CertificateException;

public class PinpadInstaller implements InstallTAListener {

    Activity activity;
    private PinpadInstallerListener listener;

    /*
     * Interface implementing the listener that will return the result
     * of TA successfully installed and opened session.
     */

    public interface PinpadInstallerListener {

        void onPinpadInstallResult(boolean installed);

    }

    public PinpadInstaller(Activity activity,
                           PinpadInstallerListener listener) {

        this.listener = listener;
        this.activity = activity;

    }

    /*
     * Use of start method here to invoke the process of installing the TA from an PinpadInstaller object
     * from outside this class.
     */
    public PinpadInstaller start() throws IllegalArgumentException {

       // String bundleID = activity.getString(R.string.taPinpad_install_ta);

        String bundleID ="dsfdf";
        int teecChoice =23423424;

       // int teecChoice = Integer.parseInt(activity.getString(R.string.taPinpad_teec_choice));

        THPAgent agent = new THPAgent(activity.getApplicationContext());
        try {
            // Require a certificate for https
            agent = agent.setServerCA("-----BEGIN CERTIFICATE-----\n" + activity.getString(R.string.ca_cert_raw) + "\n-----END CERTIFICATE-----");
            agent = agent.setServerBaseUrl(activity.getString(R.string.tam_server_url));
        } catch (CertificateException e) {
            System.out.println("Certificate Exception" + e.getMessage());
        }

        /* local Unblock, when bundled in the APP itself:
           "android.resource://{package}/raw/unblock_{suid}");
        */

        try {
            agent.installOrUpdateTA(bundleID,
                                    this,
                                    TEEMode.fromInt(teecChoice),
                                    true); /* do not used the cached TEEC choice */
        } catch (IllegalArgumentException e) {

            TextView pinpadTaTV = (TextView) activity.findViewById(R.id.pinpad_ta_installation);
            pinpadTaTV.setText("THPAgent configuration error");
            Log.e(Constants.LOG_TAG, "THPAgent configuration error. " + e.toString());
            throw e;
        }

        return this;
    }

    @Override
    public void onInstallTACompleted(Outcome outcome) {

        TextView pinpadTaTV = (TextView) activity.findViewById(R.id.pinpad_ta_installation);

        switch (outcome.getEventType()) {


            case SUCCESSFUL:

                Log.d(Constants.LOG_TAG, "onInstallTACompleted::SUCCESSFUL");
                String TAMode = outcome.getTeeClient().name();
                pinpadTaTV.setText("Pinpad TA installed in " + TAMode + " mode.");

                if (listener != null) {
                    listener.onPinpadInstallResult(true);
                }

                break;

            case ERROR:

                Log.e(Constants.LOG_TAG, "onInstallTACompleted::ERROR");
                //method returned error. To see what happened, inspect the exception thrown and perform custom logic
                Throwable errorCause = outcome.getException();
                String installationError = errorCause.getMessage();
                Log.e(Constants.LOG_TAG, installationError, errorCause);
                pinpadTaTV.setText("Error installing Pinpad TA" + installationError);

                if (listener != null) {
                    listener.onPinpadInstallResult(false);
                }
        }


    }
}
