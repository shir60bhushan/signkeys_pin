/*
 * Copyright (c) 2017 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

package com.trustonic.sample.pinpad;

import android.content.Context;

import com.trustonic.sample.pinpad.Screen.ResolutionType;
import com.trustonic.sample.pinpad.ca.PinpadCAWrapper;
import com.trustonic.util.tLog;

import java.util.ArrayList;


public class PinpadProvision {
    private static final String TAG = PinpadProvision.class.getSimpleName();

    private Context mAppContext = null;

    // keep these in sync with the TA values
    public static final int FILETYPE_IMG = 0x4F277335;
    public static final int FILETYPE_FNT = 0x78c62367;

    public PinpadProvision(Context ctxt) {
        mAppContext = ctxt;
    }

    public void onProvisionningResolution(ResolutionType res) {
    }

    public boolean isAlreadyProvisionned(ResolutionType res) {
        return PinpadCAWrapper.caPinpadIsAlreadyProvisionned(res.getTargetWidth());
    }

    /* Provision all the images on the TA, for all the resolutions */
    public boolean provisionAllAssets(ResolutionType res) {
        boolean result = false;

        tLog.i(TAG, "res = " + res.toString());

        if (ResolutionType.UNSUPPORTED == res) {
            tLog.i(TAG, "Skip provisioning for " + res.toString());
            return false;
        }

        if (isAlreadyProvisionned(res)) {
            tLog.i(TAG, "Already provisioned = " + res.toString());
            return true;
        }

        tLog.d(TAG, "res id = " + res.getTargetWidth());

        ArrayList<Integer> imageList = PinpadTUI.getInstance().getImageForResolution(res);
        onProvisionningResolution(res);
        tLog.d(TAG, "to provision " + imageList.size() + " images total");

        for (int i = 0; i < imageList.size(); i++) {
            tLog.d(TAG, "provision image with id = " + i);
            result = provisionSingleItem(res, i, FILETYPE_IMG);

            if (!result) {
                tLog.e(TAG, "error to provision image for res " + res.getTargetWidth() + " with id = " + i);
                return result;
            }
        }

        ArrayList<String> fontList = PinpadTUI.getInstance().getFontForResolution(res);
        tLog.d(TAG, "fontList.size = " + fontList.size());

        for (int i = 0; i < fontList.size(); i++) {
            tLog.d(TAG, "provision font with id = " + i);
            result = provisionSingleItem(res, i, FILETYPE_FNT);

            if (!result) {
                tLog.e(TAG, "error to provision font for res " + res.getTargetWidth() + " with id = " + i);
                return result;
            }
        }

        return result;
    }

    public boolean provisionSingleItem(ResolutionType res, int id, int binType) {
        boolean result;
        byte[] tempBuffer = null;

        switch (binType) {
            case FILETYPE_IMG:
                //lookupAsset(id)
                tempBuffer = PinpadTUI.getInstance().getRawImage(mAppContext, res, id);
                break;

            case FILETYPE_FNT:
                tempBuffer = PinpadTUI.getInstance().getRawFont(mAppContext, res, id);
                break;

            default:
                tLog.e(TAG, "Error getting item " + id + " of type " + binType);
                return false;
        }

        if (tempBuffer != null) {
            tLog.d(TAG, "Calling native caPinpadProvisionItem");
            result = PinpadCAWrapper.caPinpadProvisionItem(res.getTargetWidth(),
                                                           id,
                                                           tempBuffer,
                                                           tempBuffer.length,
                                                           binType);
        } else {
            tLog.e(TAG, "Error getting ressource " + id);
            return false;
        }

        return result;
    }
}
