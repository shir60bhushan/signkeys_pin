/*
 * Copyright (c) 2013-2017 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

package com.trustonic.sample.pinpad;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.trustonic.sample.pinpad.ca.PinpadCAWrapper;

import java.util.concurrent.atomic.AtomicBoolean;

public class Pinpad extends Activity {
    final Activity activity = this;
    private static final String TAG = Pinpad.class.getSimpleName();
    private ProgressDialog progressDialog; /* used whilst installing assets and encrypting */


    private Button pinpadBtn;
    private Button displayMsgBtn;

    private PinpadCATask pinpadCATask;
    private PinpadCATask displayMsgCATask;

    public Context ctxt;
    private static AtomicBoolean mActivityVisible = new AtomicBoolean();

    private boolean installPinpadOK = false;

    public boolean isActivityVisible() {
        return mActivityVisible.get();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        Log.d(Constants.LOG_TAG, "onCreate.");

        Log.d(Constants.LOG_TAG, "JNI-libs folder is:" +
              getApplicationContext().getApplicationInfo().nativeLibraryDir);
        super.onCreate(savedInstanceState);
        ctxt = this;
        getActionBar().setTitle(R.string.home_actionbar_title);
        getActionBar().setSubtitle(R.string.home_actionbar_subtitle);
        getActionBar().setIcon(R.drawable.t_logo);
        setContentView(R.layout.activity_start_tui_pinpad);

        progressDialog = new ProgressDialog(Pinpad.this);
        progressDialog.setMessage("Installing bundle...");
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        /* do not support cancellation */
        progressDialog.setCancelable(false);

        Log.d(Constants.LOG_TAG, "installTA...");
        /* show our progress bar */
        progressDialog.show();

        final TextView welcomeTV = (TextView) findViewById(R.id.welcome_text);

        pinpadBtn = (Button) findViewById(R.id.home_pinpad_btn);
        displayMsgBtn = (Button) findViewById(R.id.display_msg_btn);

        PinpadCAWrapper.ctxt = getApplicationContext();

        final PinpadInstaller installPinpad = new PinpadInstaller(activity, new PinpadInstaller.PinpadInstallerListener() {
            @Override
            public void onPinpadInstallResult(boolean installed) {

                /*
                 * Set the TA install flag to that which is returned from the listener.
                 */

                Pinpad.this.installPinpadOK = installed;
                welcomeTV.append("Pinpad TA installed : " + installPinpadOK + "\n");

                if (installPinpadOK) {
                    pinpadBtn.setEnabled(true);
                    displayMsgBtn.setEnabled(true);
                }

                progressDialog.dismiss();
            }
        });


        try {
            installPinpad.start();
        } catch (IllegalArgumentException e) {
            //buttonAes.setEnabled(false);
            welcomeTV.setText("THPAgent not configured correctly. See logs for more detail");
            progressDialog.dismiss();
        }

        setupUI();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        Log.d(TAG, "onResume!!");
        // Pinpad activity becomes visible
        mActivityVisible.set(true);
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        Log.d(TAG, "onPause!!");
        // Pinpad activity is no more visible
        mActivityVisible.set(false);
    }

    protected void onDestroy() {
        Log.d(Constants.LOG_TAG, "onDestroy.");
        super.onDestroy();
        // TODO is this needed?
        PinpadCAWrapper.caPinpadDisconnect();
    }

    private void setupUI() {

        pinpadBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(Constants.LOG_TAG, "Starting Pinpad sample ");
                pinpadCATask = new PinpadCATask(ctxt, PinpadCATask.TaskType.VERIFY_PIN);
                pinpadCATask.execute();
            }
        });

        displayMsgBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(Constants.LOG_TAG, "Display Message");
                displayMsgCATask = new PinpadCATask(ctxt, PinpadCATask.TaskType.DISPLAY_MESSAGE);
                displayMsgCATask.execute();
            }
        });
    }
}
