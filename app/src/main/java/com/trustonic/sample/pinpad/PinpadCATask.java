/*
 * Copyright (c) 2013-2017 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

package com.trustonic.sample.pinpad;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

import com.trustonic.sample.pinpad.ca.PinpadCAWrapper;
import com.trustonic.util.tLog;

import java.io.FileOutputStream;

public class PinpadCATask extends AsyncTask<Void, Void, String> {

    public enum TaskType {
        VERIFY_PIN,
        SET_SECURITY_INDICATOR,
        SET_PIN,
        DISPLAY_MESSAGE
    }

    private static final String TAG = PinpadCATask.class
                                      .getSimpleName();

    private Context appContext = null;
    private PinpadTUI pinpadUI = null;
    private PinpadProvision provision = null;
    FileOutputStream mFOS = null;
    private Screen screen = null;

    private TaskType task;

    public PinpadCATask(Context context, TaskType t) {
        appContext = context;
        task = t;
    }

    @Override
    protected String doInBackground(Void... arg0) {
        String result = null;

        /************************************************************/
        /* Connect to the TL Pinpad sample */
        if (PinpadCAWrapper.caPinpadConnect(appContext)) {
            tLog.d(TAG, "Connected");

            /* Get screen informations */
            if (PinpadCAWrapper.caPinpadGetResolution()) {
                tLog.d(TAG, "Get resolution");
                tLog.d(TAG, "screen width = " + PinpadCAWrapper.screenWidth
                       + " height = " + PinpadCAWrapper.screenHeight);

                screen = new Screen();

                provision = new PinpadProvision(appContext);

                if (screen.IsSupported()) {
                    /* Provision assets for this resolution to the TA Pinpad sample */
                    if (!provision.provisionAllAssets(screen.getType())) {
                        tLog.e(TAG, " provisionAllAssets failed!");
                        PinpadCAWrapper.caPinpadDisconnect();
                        tLog.d(TAG, "Disconnected");
                        result = new String("Provisioning assets failed!");
                        return result;
                    }

                    tLog.d(TAG, "Assets are provisioned");

                    /************************************************************/
                    switch (task) {
                        case VERIFY_PIN:
                            tLog.d(TAG, "Launching Pinpad TUI...");
                            //result = PinpadCAWrapper.caPinpadLaunch(provision.getStorageLocation());
                            result = PinpadCAWrapper.caPinpadVerify();
                            tLog.d(TAG, "Launch Pinpad done");
                            tLog.d(TAG, result);
                            break;

                        case SET_SECURITY_INDICATOR:
                            tLog.d(TAG, "Setting security indicator...");
                            // TODO implement in future version
                            //result = PinpadCAWrapper.caPinpadSetSecInd(provision.getStorageLocation());
                            //tLog.d(TAG, "Security indicator set done");
                            //tLog.d(TAG, result);
                            break;

                        case SET_PIN:
                            tLog.d(TAG, "Setting PIN ...");
                            // TODO implement in future version
                            //result = PinpadCAWrapper.caPinpadSetSecrets(provision.getStorageLocation());
                            //tLog.d(TAG, "PIN setting done");
                            //tLog.d(TAG, result);
                            break;

                        case DISPLAY_MESSAGE:
                            tLog.d(TAG, "Displaying security message...");
                            result = PinpadCAWrapper.caPinpadDisplayMsg();
                            tLog.d(TAG, "Displaying security message done");
                            tLog.d(TAG, result);
                            break;
                    }
                } else {
                    tLog.e(TAG, "Unsupported resolution!");
                    result = new String("Unsupported resolution!");
                    return result;
                }
            } else {
                result = new String("No communication with the TUI secure driver! Please check that the secure driver is present and that the TuiService is running.");
                tLog.d(TAG, "No communication with the TUI secure driver! Please check that the secure driver is present and that the TuiService is running.");
            }

            PinpadCAWrapper.caPinpadDisconnect();
            tLog.d(TAG, "Disconnected");
        } else {
            tLog.d(TAG, "No communication with the Secure World!");
            result = new String("No communication with the Secure World!");
        }

        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (result != null) {
            if (((Pinpad) appContext).isActivityVisible()) {
                AlertDialog.Builder alert = new AlertDialog.Builder(appContext);
                alert.setTitle(R.string.alert_title);
                alert.setMessage(result);

                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        tLog.d(TAG, "OK");
                    }
                });
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
            } else {
                Log.d(TAG, "Activity is not visible, don't show dialog!!!");
            }
        }
    }
}
