/*
 * Copyright (c) 2013-2017 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */
package com.trustonic.sample.pinpad;

import com.trustonic.sample.pinpad.ca.PinpadCAWrapper;
import com.trustonic.util.tLog;


public class Screen {
    private static final String TAG = Screen.class
                                      .getSimpleName();
    private int width;
    private int density;

    /**
     * type of resolution
     * keep these in sync with the TA values
     **/
    protected static final int LOW_RES_WIDTH        = 300;
    protected static final int MEDIUM_RES_WIDTH     = 600;
    protected static final int MIDHIGH_RES_WIDTH    = 720;
    protected static final int HIGH_RES_WIDTH       = 1080;
    protected static final int EXTRA_HIGH_RES_WIDTH = 1440;

    public enum ResolutionType {
        UNSUPPORTED (0),
        LOW (LOW_RES_WIDTH),
        MEDIUM (MEDIUM_RES_WIDTH),
        MIDHIGH (MIDHIGH_RES_WIDTH),
        HIGH (HIGH_RES_WIDTH),
        EXTRA_HIGH (EXTRA_HIGH_RES_WIDTH);

        private int targetWidth;

        ResolutionType(int width) {
            this.targetWidth = width;
        }

        int getTargetWidth () {
            return targetWidth;
        }
    }

    private ResolutionType type;

    Screen() {
        int width = PinpadCAWrapper.screenHeight;
        int height = PinpadCAWrapper.screenWidth;
        int smallestDim = width < height ? width : height;
        density = 0;
        type = ResolutionType.UNSUPPORTED;

        // Define what kind of resolution are we facing.
        tLog.d(TAG, "smallestDim=" + smallestDim);

        if (smallestDim < MEDIUM_RES_WIDTH) {
            type = ResolutionType.LOW;
            tLog.d(TAG, "Screen is type LOW");
        } else if ((smallestDim >= MEDIUM_RES_WIDTH) && (smallestDim < MIDHIGH_RES_WIDTH)) {
            type = ResolutionType.MEDIUM;
            tLog.d(TAG, "Screen is type MEDIUM");
        } else if ((smallestDim >= MIDHIGH_RES_WIDTH) && (smallestDim < HIGH_RES_WIDTH)) {
            type = ResolutionType.MIDHIGH;
            tLog.d(TAG, "Screen is type MIDHIGH");
        } else if ((smallestDim >= HIGH_RES_WIDTH) && (smallestDim < EXTRA_HIGH_RES_WIDTH)) {
            type = ResolutionType.HIGH;
            tLog.d(TAG, "Screen is type HIGH");
        } else if (smallestDim >= EXTRA_HIGH_RES_WIDTH) {
            type = ResolutionType.EXTRA_HIGH;
            tLog.d(TAG, "Screen is type EXTRA HIGH");
        } else {
            tLog.e(TAG, "Error wrong resolution!");
            type = ResolutionType.UNSUPPORTED;
        }
    }

    public ResolutionType getType() {
        return type;
    }

    public boolean IsSupported() {
        return type != ResolutionType.UNSUPPORTED;
    }
}
