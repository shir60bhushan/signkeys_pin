# Android Library

This directory is supposed to be the Android bits which will be included in the eventual Android Library

For the most part this just means the Java class which will be used in the Android app and the native libararies inside the jniLibs.

Use assemble_aar.py to populate these subdirs with the TA/CA native libs automatically and build the aar.
