/*
 * Copyright (c) 2013-2017 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

package com.trustonic.sample.pinpad.ca;

import java.io.InputStream;

import android.content.Context;
import android.util.Log;

public class PinpadCAWrapper {
    private static final String TAG = PinpadCAWrapper.class.getSimpleName();

    public static int       screenWidth;
    public static int       screenHeight;
    public static byte[]    item;
    public static int       itemLen;

    public static Context ctxt = null;

    /* Native functions */
    public static native boolean caPinpadConnect(Context appContext);
    public static native void caPinpadDisconnect();

    public static native boolean caPinpadGetResolution();

    public static native boolean caPinpadProvisionItem(int screenWidth,
                                                       int itemID,
                                                       byte[] item,
                                                       int itemLen,
                                                       int binType);
    public static native boolean caPinpadIsAlreadyProvisionned(int screenWidth);

    public static native String caPinpadDisplayMsg();
    public static native String caPinpadVerify();

    public static native byte[] caPinpadGetNativeLayout();
    public static native byte[] caPinpadGetNativeLayoutHDReady();
    public static native byte[] caPinpadGetNativeLayoutHD();
    public static native byte[] caPinpadGetNativeLayoutQuartHD();
    public static native byte[] caPinpadGetNativeLayoutQHD();

    /**
     * this is used to load the library on application startup. The library has
     * already been unpacked to the app specific folder at installation time by
     * the package manager.
     */
    static {
        System.loadLibrary("Tee");
        System.loadLibrary("CaPinpad");
    }
}
