# =============================================================================
#
# Makefile responsible for:
# - building the Java JNI wrapper - libCaPinpad.so
# - building the CA library - libstatic_caPinpad.a
#
# =============================================================================

# Do not remove this - Android build needs the definition
LOCAL_PATH  := $(call my-dir)

# =============================================================================
# TEE Client chooser lib: libTee.so
include $(CLEAR_VARS)
$(info JNI prebuilt TEE Client chooser lib)
LOCAL_MODULE    := libTee

ifeq ($(INTERNAL_MODE),)
INTERNAL_MODE=Release
endif
LOCAL_SRC_FILES := $(COMP_PATH_MobiCoreClientLib)/Bin/$(TARGET_ARCH_ABI)/$(INTERNAL_MODE)/libTee.so
include $(PREBUILT_SHARED_LIBRARY)

# =============================================================================
# CA Pinpad library: libstatic_caPinpad.a
include $(CLEAR_VARS)
LOCAL_MODULE   := static_caPinpad

# Enable logging to logcat per default
LOCAL_CFLAGS  += -DLOG_ANDROID
# TBASE_API_LEVEL >=9 for TUI/Proxy binding
LOCAL_CFLAGS  += -DTBASE_API_LEVEL=9


# Android.mk variables with prefix "TRUSTED_APP_DEP_PUBLIC_DIR_" will be
# used in the THP build scripts to indicate this CA's TA dependencies. You
# should also manually add them in the appropriate LOCAL_C_INCLUDES, as required.
# This directory location must follow the convention the THP build scripts
# expect e.g. ${TRUSTED_APP_DEP_PUBLIC_DIR_<TA>}/../../../src/makefile.mk exists
# This directory location could be build generated
TRUSTED_APP_DEP_PUBLIC_DIR_TAPinpad := $(KINIBI_DEV_KIT_PATH)/samples/Pinpad/build/taPinpad/public

LOCAL_C_INCLUDES += \
    ${TRUSTED_APP_DEP_PUBLIC_DIR_TAPinpad} \
    ${LOCAL_PATH}/../inc \
    $(COMP_PATH_MobiCoreClientLib)/Public/GP \
    $(COMP_PATH_MobiCoreClientLib)/Public

LOCAL_SRC_FILES += caPinpad.c

LOCAL_SHARED_LIBRARIES := libTee

include $(BUILD_STATIC_LIBRARY)

# =============================================================================
# JNI module: libCaPinpad.so
include $(CLEAR_VARS)
LOCAL_MODULE    := CaPinpad

# Enable logging to logcat per default
LOCAL_CFLAGS  += -DLOG_ANDROID
# TBASE_API_LEVEL >=9 for TUI/Proxy binding
LOCAL_CFLAGS  += -DTBASE_API_LEVEL=9

LOCAL_SRC_FILES += client-jni.cpp

LOCAL_C_INCLUDES += \
    ${TRUSTED_APP_DEP_PUBLIC_DIR_TAPinpad} \
    ${LOCAL_PATH}/../inc \
    $(COMP_PATH_MobiCoreClientLib)/Public/GP \
    $(COMP_PATH_MobiCoreClientLib)/Public


LOCAL_LDLIBS += -llog
LOCAL_STATIC_LIBRARIES := static_caPinpad
LOCAL_SHARED_LIBRARIES := libTee

include $(BUILD_SHARED_LIBRARY)
