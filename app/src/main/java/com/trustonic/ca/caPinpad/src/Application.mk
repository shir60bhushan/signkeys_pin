# =============================================================================
#
# Main build file defining the project modules and their global variables.
#
# =============================================================================

# Don't remove this - mandatory
APP_PROJECT_PATH := $(call my-dir)

# Don't optimize for better debugging
APP_OPTIM := debug

# Position Independent Executable
APP_PIE := true

# build ARM32 and ARM64 libraries
APP_ABI := armeabi-v7a arm64-v8a

APP_PLATFORM := android-17
