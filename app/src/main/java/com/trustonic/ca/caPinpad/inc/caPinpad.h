/*
 * Copyright (c) 2016-2017 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

#ifndef _CA_PINPAD_H_
#define _CA_PINPAD_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <tee_client_api.h>

#define MAX_FILE_SIZE 200000
TEEC_Result OpenSession();
TEEC_Result CloseSession();
bool securePinpadConnect();
bool securePinpadProvisionItem(int screenWidth, int itemID, char *data,
                               uint32_t dataLen, int binType);
TEEC_Result securePinpadIsAlreadyProvisionned(uint32_t screenWidth, bool *isProvisionned);
TEEC_Result securePinpadGetResolution(uint32_t *width, uint32_t *height);
const char *securePinpadDisplayMsg();
const char *securePinpadVerify(uint32_t); //FIXME no param used

#ifdef __cplusplus
}
#endif
#endif // _CA_PINPAD_H_
